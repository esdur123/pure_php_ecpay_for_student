<?php
/**
 * 編輯商品資料
 */

    // 使用session
    session_start();
    if($_SESSION['loginuser_account']!='ADMIN'){ // 限制只有ADMIN這個帳號能往下走
        echo "只有ADMIN能修改";
        exit;
    }


	// 把傳過來的值 先處理
    $post_edit_id       = $_POST['edit_id']; // 商品id(資料庫 product的id)
	$post_title         = strtoupper($_POST['title']); // 商品名稱 (強制大寫strtoupper)
	$post_descript      = $_POST['descript']; // 商品描述
    $post_specification = $_POST['specification']; //商品尺寸 (JSON)


    // 連線資料庫
    include_once 'dbconnect.php';

    // 修改商品的SQL(沒有圖片版)
    $sql = "UPDATE `".$db_name."`.`product` 
            SET title='".$post_title."', descript='".$post_descript."',specification='".$post_specification."'  
            WHERE id='".$post_edit_id."';
           "; // sql指令, 取得所有的user資料庫資訊

    // 如果有上傳圖片
    if(isset($_FILES['image'])){

        $name_arr = explode('.', $_FILES['image']['name']); // 把AAA.JPG用逗點拆開丟入兩個陣列[0][1]
        $sub_name = $name_arr[count($name_arr)-1]; // 找出陣列[1] 就是找出副檔名的意思
        $sub_name = strtolower($sub_name); // 取出副檔名 強制小寫 (strtolower)
        if(!($sub_name=='jpg'||$sub_name=='jpeg'||$sub_name=='png')){ // 限制圖片附檔名類型
             echo "圖片副檔名jpg,jpeg,png限定";
             exit;
        }

        $image = 'image/' . $_FILES['image']['name']; // 圖片完整位置(要放在image資料夾內)
        move_uploaded_file($_FILES['image']['tmp_name'], $image); // 圖片存檔至指定位置

        // 修改商品的SQL(有圖片版)
        $sql = "UPDATE `".$db_name."`.`product` 
                SET title='".$post_title."', descript='".$post_descript."',specification='".$post_specification."',image='".$image."'  
                WHERE id='".$post_edit_id."';
               ";
    }    


    // 執行SQL
    mysqli_query($conn, $sql) or die(mysqli_error()); 

    // 回傳AJAX的結果給前端
    echo "修改成功";

?>

