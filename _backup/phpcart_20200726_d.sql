-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 伺服器版本:                        10.4.10-MariaDB - mariadb.org binary distribution
-- 伺服器操作系統:                      Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 傾印  表格 cestudzp_phpcart.orders 結構
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '會員ID',
  `total_price` int(11) DEFAULT NULL COMMENT '總價',
  `cart_json` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '購物車內容Json',
  `return_json` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '金流回傳的資訊',
  `payment_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '付款方式',
  `payment_fee` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '綠界手續費',
  `obtained_at` datetime DEFAULT NULL COMMENT '訂單建立時間',
  `paid_at` datetime DEFAULT NULL COMMENT '金流收款時間(null代表沒收到款)',
  `order_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '購買人姓名',
  `order_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '購買人電話',
  `order_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '購買人地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- 正在傾印表格  cestudzp_phpcart.orders 的資料：~0 rows (約數)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- 傾印  表格 cestudzp_phpcart.product 結構
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品名稱',
  `descript` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品描述',
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品圖片網址',
  `specification` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品尺寸Json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- 正在傾印表格  cestudzp_phpcart.product 的資料：~4 rows (約數)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `title`, `descript`, `image`, `specification`) VALUES
	(1, '兒童刀叉組1', '商品描述 111111111111', 'image/product_1.jpeg', '[{"size":"大","price":"300"},{"size":"客製","price":"400"},{"size":"a","price":"88"}]'),
	(2, '安全餐具2', '商品描述 here  22222', 'image/product_2.jpeg', '[{"size":"XL","price":"2100"},{"size":"M","price":"2200"},{"size":"S","price":"2300"}]'),
	(3, '天鵝玩具CC', '商品描述 here 3333333333333333333333333333', 'image/product_3.jpeg', '[{"size":"XXL","price":"1001"},{"size":"XL","price":"2001"},{"size":"M","price":"3001"},{"size":"X","price":"4001"},{"size":"XS","price":"5001"}]'),
	(4, '兔子玩偶DDD', '商品描述 44444444444444444444444444444444444', 'image/product_4.jpeg', '[{"size":"A","price":"666"},{"size":"B","price":"555"},{"size":"C","price":"44"}]');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- 傾印  表格 cestudzp_phpcart.user 結構
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '會員ID',
  `account` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員帳號',
  `password` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員密碼',
  `is_verified` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '是否通過註冊',
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員姓名',
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員手機',
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員地址',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員Email',
  `verified_phone` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '是否通過手機驗證',
  `verified_email` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '是否通過信箱驗證',
  `vcode_email` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '會員驗證Email碼',
  `vcode_phone` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'none' COMMENT '會員驗證簡訊碼',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 正在傾印表格  cestudzp_phpcart.user 的資料：~1 rows (約數)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `account`, `password`, `is_verified`, `username`, `phone`, `address`, `email`, `verified_phone`, `verified_email`, `vcode_email`, `vcode_phone`) VALUES
	(1, 'ADMIN', '123456', '1', NULL, NULL, NULL, NULL, '0', '0', NULL, 'none'),
	(36, 'GEAGEA', '123456', '0', 'aegswegeg', 'none', 'aeegeg', 'ESDUR123@GMAIL.COM', '0', '0', '15957812860174', '6611');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
