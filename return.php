<?php
/**
 * 接收綠界回傳的收款結果
 */

    // 時區
    date_default_timezone_set("Asia/Taipei");

    // 綠界會用post的方式傳一包東西給我們     
    $get_post = $_POST; // 把那包post接起來
    $get_post = json_encode($get_post); // 把那包post轉成json 等等要原封不動的存到 return_json裡面
    $return_info = 'nothing'; // 綠界會把他的回傳結果另外包成一個加密過的東西 等等用$return_info接起來 先給個初值 nothing

    //載入SDK(路徑可依系統規劃自行調整)
    include('defined.php'); // 這行是我們自己加的, 取得統一寫好的綠界各種商店資訊key, iv 商店號碼 那些..
    include('ECPay.Payment.Integration.php'); // 綠界提供的檔案, 等等ECPay_AllInOne()和CheckOutFeedback()這兩個function, 來自這隻檔案
    try {
        
        // 準備把綠界的加密資訊 解密, 他就是要這樣解密
        $obj = new ECPay_AllInOne();
        $obj->HashKey     = $ECPayInfo_HashKey;                                           //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = $ECPayInfo_HashIV;                                            //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = $ECPayInfo_MerchantID;                                        //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = $ECPayInfo_EncryptType;                                       //CheckMacValue加密類型，請固定填入1，使用SHA256加密

        // 解密
        $return_info =  $obj->CheckOutFeedback();
          
    } catch (Exception $e) {
        echo $e->getMessage();
        exit;
    }         

    // 把解密資訊轉成json
    $json_return_info = json_encode($return_info);
    

    // 連線資料庫
    include_once 'dbconnect.php';   // 連線資料庫 


    // 如果解密資訊 告知付款成功 (就是RtnCode=1) 那我們要依照當時我們自己在CustomField1裡面紀錄的訂單id, 去修改訂單狀態
    // paid_at(付款時間 紀錄現在時間), 回存原始的綠界回傳資訊(return_json), 付款方式(payment_type), 綠界的付款收費(payment_fee)
    if($return_info['RtnCode']=='1'){ 

        $update_sql = "UPDATE `".$db_name."`.`orders` 
                SET paid_at='".date("Y-m-d H:i:s")."', return_json='".$json_return_info."', payment_type='".$return_info['PaymentType']."', payment_fee='".$return_info['PaymentTypeChargeFee']."'   
                WHERE id='".$return_info['CustomField1']."';
               "; 

    // 如果如果解密資訊 回傳付款不成功 那資料庫orders的付款時間(paid_at) 就不要存
    } else {

        $update_sql = "UPDATE `".$db_name."`.`orders` 
                SET return_json='".$json_return_info."', payment_type='".$return_info['PaymentType']."', payment_fee='".$return_info['PaymentTypeChargeFee']."'   
                WHERE id='".$return_info['CustomField1']."';
               ";         

    }


    // 執行sql
    mysqli_query($conn, $update_sql) or die(mysqli_error()); 


?>

