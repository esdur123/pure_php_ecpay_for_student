<?php  
    // --------- php部分開始 ----------

    // 連線資料庫
    include_once 'dbconnect.php';

    // 使用session
    session_start();

    // 檢查是否有登入的session
    $session_account = '';
    $session_id = '0';
    $userInfo = '';
    if ( isset($_SESSION['loginuser_account']) && $_SESSION['loginuser_account']!= "") { // 如果有取得session登入帳號
        $session_account = $_SESSION['loginuser_account']; // 取得登入帳號
        $session_id = $_SESSION['loginuser_id']; // 取得登入id

        // 根據id 取得會員資訊 (session_id 就是 user表的id)
        $user_sql = "SELECT * FROM `".$db_name."`.`user` where `id` = '".$session_id."'"; 
        $user_result = mysqli_query($conn, $user_sql) or die(mysqli_error()); // 執行sql 
        while ($user_row = mysqli_fetch_object($user_result)) {  
              $user_info = $user_row; // 取得會員資訊
        }         
        $userInfo = json_encode($user_info); // 會員資訊轉json
    }    

    // 商品
    $productArray = array();
    $product_sql = "SELECT * FROM `".$db_name."`.`product` "; // sql取得目前所有商品
    $product_result = mysqli_query($conn, $product_sql) or die(mysqli_error()); // 執行sql 
    while ($product_row = mysqli_fetch_object($product_result)) {  // 根據取得的商品　把商品一件一件丟到prodrcyArray裡面
         array_push($productArray, $product_row);
    }
    for ($i=0; $i < count($productArray); $i++) { // 因為商品的specification這欄存的是尺寸的json 所以要一個一個把json轉回array包obj
        $productArray[$i]->specification = json_decode($productArray[$i]->specification);        
    }
    $productArray = json_encode($productArray); // 最終把撈到的商品資訊　轉為json, 等等下面javascript ready的時候要接這個值

    // 訂單
    $orderArray = array();
    if(!$session_account==''){ // 有登入　並且不是ADMIN　的訂單

      // 一般會員的訂單
      if($session_account!='ADMIN'){
        // 取得訂單的sql (這個會員的訂單　所以user_id要指定)
        $order_sql = "SELECT *, `".$db_name."`.`orders`.id as order_id FROM `".$db_name."`.`orders` where user_id ='".$session_id."' order by obtained_at DESC;";        

      // admin看全部訂單
      } else {
        // 取得訂單的sql (這個會員的訂單　所以user_id要指定)
        // 因為要根據訂單上的 user_id取得 會員account 所以要 left join 另一張表 user
        $order_sql = "SELECT *, `".$db_name."`.`orders`.id as order_id , `".$db_name."`.`user`.account FROM `".$db_name."`.`orders` left join `".$db_name."`.`user` on `".$db_name."`.`orders`.user_id =  `".$db_name."`.`user`.id order by obtained_at DESC;";  
      }

      $order_result = mysqli_query($conn, $order_sql) or die(mysqli_error()); // 執行sql    
      while ($order_row = mysqli_fetch_object($order_result)) {
           array_push($orderArray, $order_row); // 根據取得的訂單　把訂單一個一個丟到orderArray裡面
      }
      for ($i=0; $i < count($orderArray); $i++) { // 因為訂單的cart_json和return_json這欄存的是json 所以要一個一個把json轉回array包obj 
          $orderArray[$i]->cart_json = json_decode($orderArray[$i]->cart_json); // 訂單裡存放商品的json       
          $orderArray[$i]->return_json = json_decode($orderArray[$i]->return_json); // 訂單裡存放綠界回傳結果的json
      }           
    }
    $orderArray = json_encode($orderArray);// 最終把撈到的訂單資訊　轉為json, 等等下面javascript ready的時候要接這個值
    //var_dump($orderArray);

    // --------- php部分結束 ----------
?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>購物中心</title>

        <!--引用網路資源 bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" />
        <!--自己定義的CSS Class -->
        <link href="css/myCss.css" rel="stylesheet" />
    </head>

    <!-- body 開始 (body初始先隱藏 等jquery ready之後再一起顯示) -->
    <body id="body_div" style="display: none;">

        <!-- 上面黑色區域(nav) 開始-->
        <nav class="navbar navbar-inverse">
            <div class="container" style="width:100%">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">購物中心</a>
                </div>
                <!-- nav右邊的顯示 開始 -->
                <div class="navbar-collapse collapse" id="nav_div">
                </div>
                <!-- nav右邊的顯示 結束 -->
            </div>
        </nav>
        <!-- 上面黑色區域(nav) 結束-->


        <!-- 上面灰色區域 開始-->
        <div class="jumbotron jumbotron-fluid">
          <div class="container">
            <h1>購物中心</h1>
            <h3>歡迎您..</h3>
          </div>
        </div>
        <!-- 上面灰色區域 結束-->


        <!-- 白色區域 開始 ,container讓整體往中間擠-->
        <div class="container">

            <!-- 第1組商品div 開始 (用row包網格col)-->
            <div class="row">
                <!-- 左邊的商品圖片 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productPicDiv"> 
                    <img src="" class="imageProduct" id="image_1">
                </div>
    

                <!-- 商品描述 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> 
                    <div class="productDescriptDiv" id="descript_1">
                        商品描述 here
                    </div>                                   
                </div>    
                  
                
                <!-- 右邊的商品按鈕 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productBtnDiv">      
                    <div class="productTitle" id="title_1">兒童刀叉組</div>                

                    <div class="form-group">
                      <label for="sel1">選擇尺寸</label>
                      <select class="form-control product_specific" id="specific_1"> 
<!--                         <option>大</option>
                        <option>中</option>
                        <option>小</option> -->
                      </select>
                    </div>                    

                    <div class="form-group">
                      <label for="sel1">單價</label>
                      <ul class="list-group">
                        <li class="list-group-item product_price" id="price_1">未定</li>
                      </ul>
                    </div>  

                    <div class="form-group">
                      <label for="sel1">選擇數量</label>
                      <input class="form-control" type="number" id="amount_1" min="0" max="10" value="1">
                    </div>       

                    <button type="button" class="btn btn-primary hide_admin" value="1" buy="0" onclick="addToCart(this)">加入購物車</button>
                    <button type="button" class="btn btn-success hide_admin" value="1" buy="1" onclick="addToCart(this)">直接購買</button>
                    <button type="button" class="btn btn-warning show_admin" value="1" style="display: none;" onclick="ModalEdit(this)">編輯</button>
                </div>                               
            </div>
            <!-- 第1組商品div 結束 -->



            <!-- 第2組商品div 開始 (用row包網格col)-->
            <div class="row">
                <!-- 左邊的商品圖片 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productPicDiv"> 
                    <img src="" class="imageProduct" id="image_2">
                </div>
    

                <!-- 商品描述 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> 
                    <div class="productDescriptDiv" id="descript_2">
                        商品描述 here
                    </div>                                   
                </div>    
                  
                
                <!-- 右邊的商品按鈕 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productBtnDiv">                      
                    <div class="productTitle" id="title_2">安全餐具</div>

                    <div class="form-group">
                      <label for="sel1">選擇尺寸</label>
                      <select class="form-control product_specific" id="specific_2">
<!--                         <option>大</option>
                        <option>中</option>
                        <option>小</option> -->
                      </select>
                    </div>                    

                    <div class="form-group">
                      <label for="sel1">單價</label>
                      <ul class="list-group">
                        <li class="list-group-item product_price" id="price_2">未定</li>
                      </ul>
                    </div>  

                    <div class="form-group">
                      <label for="sel1">選擇數量</label>
                      <input class="form-control" type="number" id="amount_2" name="points" min="0" max="10" value="1">
                    </div>   

                    <button type="button" class="btn btn-primary hide_admin" value="2" buy="0" onclick="addToCart(this)">加入購物車</button>
                    <button type="button" class="btn btn-success hide_admin" value="2" buy="1" onclick="addToCart(this)">直接購買</button>
                    <button type="button" class="btn btn-warning show_admin" value="2" style="display: none;" onclick="ModalEdit(this)">編輯</button>         
                </div>                               
            </div>
            <!-- 第2組商品div 結束 -->


            <!-- 第3組商品div 開始 (用row包網格col)-->
            <div class="row">
                <!-- 左邊的商品圖片 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productPicDiv"> 
                    <img src="" class="imageProduct" id="image_3">
                </div>
    

                <!-- 商品描述 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> 
                    <div class="productDescriptDiv" id="descript_3">
                        商品描述 here
                    </div>                                   
                </div>    
                  
                
                <!-- 右邊的商品按鈕 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productBtnDiv">      
                    <div class="productTitle" id="title_3">天鵝玩具</div>                         

                    <div class="form-group">
                      <label for="sel1">選擇尺寸</label>
                      <select class="form-control product_specific" id="specific_3">
<!--                         <option>大</option>
                        <option>中</option>
                        <option>小</option> -->
                      </select>
                    </div>                    

                    <div class="form-group">
                      <label for="sel1">單價</label>
                      <ul class="list-group">
                        <li class="list-group-item product_price" id="price_3">未定</li>
                      </ul>
                    </div>  

                    <div class="form-group">
                      <label for="sel1">選擇數量</label>
                      <input class="form-control" type="number" id="amount_3" name="points" min="0" max="10" value="1">
                    </div>      

                    <button type="button" class="btn btn-primary hide_admin" value="3" buy="0" onclick="addToCart(this)">加入購物車</button>
                    <button type="button" class="btn btn-success hide_admin" value="3" buy="1" onclick="addToCart(this)">直接購買</button>     
                    <button type="button" class="btn btn-warning show_admin" value="3" style="display: none;" onclick="ModalEdit(this)">編輯</button> 
                </div>                               
            </div>
            <!-- 第3組商品div 結束 -->


            <!-- 第4組商品div 開始 (用row包網格col)-->
            <div class="row">
                <!-- 左邊的商品圖片 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productPicDiv"> 
                    <img src="" class="imageProduct" id="image_4">
                </div>
    

                <!-- 商品描述 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> 
                    <div class="productDescriptDiv" id="descript_4">
                        商品描述 here
                    </div>                                   
                </div>    
                  
                
                <!-- 右邊的商品按鈕 -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productBtnDiv">  
                    <div class="productTitle" id="title_4">兔子玩偶</div>                         

                    <div class="form-group">
                      <label for="sel1">選擇尺寸</label>
                      <select class="form-control product_specific" id="specific_4">
<!--                         <option>大</option>
                        <option>中</option>
                        <option>小</option> -->
                      </select>
                    </div>                    

                    <div class="form-group">
                      <label for="sel1">單價</label>
                      <ul class="list-group">
                        <li class="list-group-item product_price" id="price_4">未定</li>
                      </ul>
                    </div>  

                    <div class="form-group">
                      <label for="sel1">選擇數量</label>
                      <input class="form-control" type="number" id="amount_4" name="points" min="0" max="10" value="1">
                    </div>     

                    <button type="button" class="btn btn-primary hide_admin" value="4" buy="0" onclick="addToCart(this)">加入購物車</button>
                    <button type="button" class="btn btn-success hide_admin" value="4" buy="1" onclick="addToCart(this)">直接購買</button>
                    <button type="button" class="btn btn-warning show_admin" value="4" style="display: none;" onclick="ModalEdit(this)">編輯</button>                    
                </div>                               
            </div>
            <!-- 第4組商品div 結束 -->

  
            <!-- 註冊視窗 (平時隱藏 呼叫的時候才彈窗) 開始 -->
            <div class="container">
                <div id="modal_register" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">註冊</h4>
                        </div>
                        <div class="modal-body">
                                                
                            <div class="form-group">
                              <label for="account">帳號:</label>
                              <input type="text" class="form-control" id="register_account" maxlength="20">
                            </div>

                            <div class="form-group">
                              <label for="password">密碼:</label>
                              <input type="password" class="form-control" id="register_password" maxlength="20">
                            </div>

                            <div class="form-group">
                              <label for="username">姓名:</label>
                              <input type="text" class="form-control" id="register_username" maxlength="20">
                            </div>

                            <div class="form-group" style="text-align: left;">
                               <label for="register_mail">Email:</label>
                               <input type="text" class="form-control"  name="register_mail" id="register_mail" maxlength="20" placeholder="">
                             </div>  

                            <div class="form-group">
                              <label for="phone">手機:</label>
                              <input type="text" class="form-control" id="register_phone" maxlength="10" placeholder="09xxxxxxxx">
                            </div>

                            <div class="form-group">
                              <label >地址:</label><br />
                              <textarea class="form-control" rows="3" id="register_address" maxlength="50"></textarea>
                            </div>
                                        


                            <label >請擇一驗證:</label><br />
                            <table class="table table-bordered size_20" >                              
                              <tbody id="">     
                                <tr>
                                  <td class="width_50" id="td_btn_email"><button type="button" class="btn btn-success" reg_type="email" onclick="Register(this)">Email驗證註冊</button></td>
                                </tr>
                                <tr>                                  
                                  <td class="width_50" id="td_btn_sms">
                                    <button type="button" class="btn btn-success" reg_type="phone" onclick="Register(this)">手機驗證註冊</button> 
                                    <br />
                                    <div class="form-group">                                      
                                      <input type="text" class="form-control" id="confirm_phone_vcode" maxlength="4" placeholder="驗證碼">
                                      <button type="button" class="btn btn-default" onclick="confirmSMS()">完成手機驗證註冊</button>
                                    </div>                                        
                                  </td>

    
                                </tr>                                
                              </tbody>
                            </table>                                                                                      

                        </div>
                        <div class="modal-footer">
                          <!-- <button type="text" class="btn btn-success" onclick="Register()">註冊</button>    -->
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                </div>      
            </div> 
            <!-- 註冊視窗 (平時隱藏 呼叫的時候才彈窗) 結束-->      


            <!-- 購物車視窗 (平時隱藏 呼叫的時候才彈窗) 開始 -->
            <div class="container">
                <div id="modal_cart" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">購物車</h4>
                        </div>
                        <div class="modal-body">

                          <!-- 購物車傳給金流的php位址 sample_All_CreateOrder.php 傳送方式為post -->
                          <form id="payForm" action="sample_All_CreateOrder.php" method="post" >
                            <table class="table table-bordered" >
                                <tbody id="tbody_cart">     
                                      <!-- 購物車內容會出現在這邊 -->
                                </tbody>
                            </table>  

                            <div class="cart_footer">
                              <div class="total_price"  >
                                總價 :                                
                              </div>

                              <hr>

                              <!-- 真正要post出去的價格　隱藏不顯示 -->
                              <input type="text" class="form-control"  name="cart_price" id="cart_price" style="display: none;">
                              <!-- 真正要post出去的會員ID　隱藏不顯示 -->
                              <input type="text" class="form-control"  name="cart_user_id" id="cart_user_id" style="display: none;">
                              <!-- 真正要post出去的商品json　隱藏不顯示 -->
                              <textarea class="form-control" rows="10"  name="cart_json" id="cart_json" style="display: none;"></textarea>
                              <br />                            
                              <button type="button" class="btn btn-success" onclick="submitCart()">結帳</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                              <div class="form-group" style="text-align: left;">
                                <label for="cart_username">姓名:</label>
                                <input type="text" class="form-control"  name="cart_username" id="cart_username" maxlength="20" >
                              </div>                              

                              <div class="form-group" style="text-align: left;">
                                <label for="cart_phone">手機:</label>
                                <input type="text" class="form-control"  name="cart_phone" id="cart_phone" maxlength="10" placeholder="09xxxxxxxx">
                              </div>  

                              <div class="form-group" style="text-align: left;">
                                <label for="cart_address">地址:</label>
                                <textarea class="form-control"  name="cart_address" id="cart_address" maxlength="50" > </textarea>
                              </div>                                

                            </div>
                          </form>

                        </div>
                        <div class="modal-footer">

                        </div>

                    </div>
                </div>      
            </div> 
            <!-- 購物車視窗 (平時隱藏 呼叫的時候才彈窗) 結束-->    



            <!-- 登入視窗 (平時隱藏 呼叫的時候才彈窗) 開始 -->
            <div class="container">
                <div id="modal_login" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">登入</h4>
                        </div>
                        <div class="modal-body">
                                                
                            <div class="form-group">
                              <label for="account">帳號:</label>
                              <input type="text" class="form-control" id="login_account" >
                            </div>

                            <div class="form-group">
                              <label for="password">密碼:</label>
                              <input type="password" class="form-control" id="login_password" >
                            </div>

                        </div>
                        <div class="modal-footer">
                          <button type="text" class="btn btn-success" onclick="Login()">登入</button>   
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                </div>      
            </div> 
            <!-- 登入視窗 (平時隱藏 呼叫的時候才彈窗) 結束-->    


            <!-- 訂單視窗 (平時隱藏 呼叫的時候才彈窗) 開始 -->
            <div class="container">
                <div id="modal_orders" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">訂單</h4>
                        </div>
                        <div class="modal-body">
                                                
                            <table class="table table-bordered" >                              
                              <tbody id="tbody_orders">     
                                  <!-- 訂單會出現在這邊 -->
                              </tbody>
                            </table>  

                            
                            <div id="pager_orders">       
                              <!-- 訂單的頁面 會出現在這邊 -->                       
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                </div>      
            </div> 
            <!-- 訂單視窗 (平時隱藏 呼叫的時候才彈窗) 結束-->   


            <!-- 編輯視窗 (平時隱藏 呼叫的時候才彈窗) 開始 -->
            <div class="container">
                <div id="modal_edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">編輯</h4>
                        </div>
                        <div class="modal-body">
                                                
                              <table class="table table-bordered">
                                <tbody>     
                                  <tr>
                                    <td><h5>圖片</h5></td>
                                    <td>

                                      <label class="btn btn-upload">                
                                          <input id="upload_edit_img" type="file" style="display: none;">
                                          <span style="font-size: 20px;">修改</span>
                                          <img id="modal_edit_image" src="" class="img-responsive" style="max-width: 150px;max-height: 150px;">
                                      </label>                                                              
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><h5>商品名稱</h5></td>
                                    <td>
                                        <input type="text" class="form-control" id="modal_edit_title">                                       
                                    </td>
                                  </tr>          
                                  <tr id=''>
                                    <td><h5>商品描述</h5></td>
                                    <td>
                                        <textarea class="form-control" rows="5" id="modal_edit_descript" style="max-width: 100%;"></textarea>
                                    </td>
                                  </tr>        
                                  <tr id=''>
                                    <td><h5>尺寸/單價</h5></td>
                                    <td>
                                        <div id="div_img_create">
                                          <!-- 編輯的那些單價尺寸　會秀在這邊 -->

                                <!--             <div class="div_size_edit">
                                                <input type="text" class="form-control editInputs edit_size" placeholder="尺寸">
                                                <input type="text" class="form-control editInputs edit_price" placeholder="單價">
                                                <div class="div_del"><button type="button" class="btn btn-danger edit_del">刪除</button></div>
                                            </div>  -->           
                                        </div>
                                        <span id="add_create">+</span>
                                    </td>
                                  </tr>                                                                               
                                </tbody>
                              </table>  

                        </div>
                        <div class="modal-footer">
                          <button type="text" class="btn btn-primary" onclick="Edit()">確認修改</button>   
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                </div>      
            </div> 
            <!-- 登入視窗 (平時隱藏 呼叫的時候才彈窗) 結束-->   


        </div>      
        <!-- 白色區域 結束-->    


        <!-- 引用.js檔 要寫在body的最下方-->
        <!-- 引用網路上的 jquery .js -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <!-- 引用網路上的 bootstrap .js -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <!-- 這個網頁的的javaScript程式 開始 -->        
        <script>
            
            // 先把php傳來的登入值傳給javaScript
            var session_account = '<?php echo $session_account; ?>';    
            var session_id      = '<?php echo $session_id; ?>';    

            // 把會員資訊 從php傳至javaScript
            var get_user_info = '';
            if(session_id!=0){
              get_user_info = '<?php echo $userInfo; ?>';    
              get_user_info = JSON.parse(get_user_info); // 商品資訊從json轉回object      
            }

            // 把商品資料 從php傳至javaScript
            var get_product_array = '<?php echo $productArray; ?>';
            get_product_array = JSON.parse(get_product_array); // 商品資訊從json轉回array包object         


            // 把訂單資料 從php傳至javaScript
            var get_order_array = '<?php echo $orderArray; ?>';   
            // 因為訂單可能是空的　所以要用try catch
            try {
               get_order_array = JSON.parse(get_order_array);
            }
            catch (e) {
                get_order_array = [];
            }
            //console.log(get_order_array);

            // jQuery當網站讀取完畢 要做的事 開始     
            var groupProduct = [];
            $(document).ready(function () {
                    
                // 第一個for迴圈: 商品 把商品資訊顯示出來(不含尺寸)
                for (var i = 0; i < get_product_array.length; i++) {
                    // 顯示圖片
                    var id_image = "#image_"+get_product_array[i].id;
                    $(id_image).attr("src", get_product_array[i].image);

                    // 顯示商品名稱 title_4
                    var id_title = "#title_"+get_product_array[i].id;
                    $(id_title).text(get_product_array[i].title);

                    // 顯示商品描述 descript_4
                    var id_descript = "#descript_"+get_product_array[i].id;
                    $(id_descript).html(get_product_array[i].descript);

                    // 第二個for迴圈 , 尺寸, 每個商品都要掃一次他的尺寸
                    var id_specific = "#specific_"+get_product_array[i].id; // 根據商品id 決定等等要改ie="specific_?"幾號的html
                    var html = '';
                    var count_found = 0;
                    var specific_array = get_product_array[i].specification;                    
                    for (var j = 0; j < specific_array.length; j++) {   
                        if(j==0){
                            var id_price = "#price_"+get_product_array[i].id; 
                           $(id_price).text(specific_array[j].price); // 初始價格(就是第一個尺寸的價格)
                        }                        

                        html += '<option value="'+specific_array[j].price+'">'+specific_array[j].size+'</option>';                            
                    }

                    $(id_specific).html(html);

                    // 用編號把商品資訊塞進 groupProduct
                    groupProduct[get_product_array[i].id] = get_product_array[i];                    
                }

                // 當尺寸的下拉選單有改變的時候
                $(".product_specific").change(function(){  
                    var index =$('.product_specific').index(this);  // 找到目前改變的是.product_specific的第幾個(class)
                    var value = $(this).val(); // 目前改變的尺寸的價格
                    $(".product_price").eq(index).text(value);  // eq就是第幾個, 也就是第幾個 product_specific calass要改變值
                                                                // 所以改變尺寸時 他對應的顯示價格會改變
                });

                // 如果未登入
                if(session_account==''){;
                    // 上方顯示註冊和登入
                    showUnloginNav();
                }

                // 如果已登入
                if(session_account!=''){
                    // 上方顯示帳號和登出
                    showLoginNav();

                    // 如果登入者是admin 顯示編輯
                    if(session_account=='ADMIN'){
                        $(".show_admin").show();
                        $(".hide_admin").hide();                     
                    // 如果登入者是其他人 隱藏編輯
                    } else {
                        $(".show_admin").hide();
                        $(".hide_admin").show();

                        // 會員Modal
                        $("#user_account").text(get_user_info.account);
                        $("#user_mail").text(get_user_info.email);
                        $("#user_phone").text(get_user_info.phone);

                        // 根據資料庫 顯示是否驗證Email     
                        var is_veriried_mail = '<span class="color_red">信箱未驗證</span>';                        
                        if(get_user_info.verified_email=='1'){
                          is_veriried_mail = '<span class="color_green">信箱已驗證</span>';  
                          $("#td_btn_email").html(""); // 取消驗證信箱按鈕
                        }
                        $("#user_mail_verified").html(is_veriried_mail);

                        // 根據資料庫 顯示是否驗證手機
                        var is_veriried_phone = '<span class="color_red">簡訊未驗證</span>';
                        if(get_user_info.verified_phone=='1'){
                          is_veriried_phone = '<span class="color_green">簡訊已驗證</span>';  
                          $("#td_btn_sms").html(""); // 取消 驗證簡訊按鈕
                          $("#td_div_sms").html(""); // 取消 手機驗證碼 div
                        }                        
                        $("#user_phone_verified").html(is_veriried_phone);                         
                    }                   
                }                

                // 編輯商品圖片 圖片改變時
                $("#upload_edit_img").change(function(){
                  readURL(this, '#modal_edit_image');　// 讀取圖檔
                }); 


                // 按下新增尺寸時
                $("#add_create").click(function(){
                  plusSpecification('',''); // 新增一個空的尺寸
                });                

                // 檢查目前的購物車情況
                checkCurrentCart();

                // 顯示body畫面）(開始這個是隱藏的)
                $("#body_div").show();                

            });
            // jQuery當網站剛讀取完畢時要做的事 結束     


            // 加入購物車
            function addToCart(obj){
                
                var id = $(obj).attr("value"); // 取得value, 這邊我們已經寫死了, 每個value代表第幾個商品
                var specific_id = "#specific_"+id; // 找出　第幾個尺寸的id
                var size = $(specific_id).find("option:selected").text(); // 取得目前第幾個尺寸　的商品名稱
                var price = $(specific_id).val(); // 第幾個尺寸的　價格
                var title_id = "#title_"+id; // 找出　第幾個商品名稱的id
                var title = $(title_id).text();　// 找出　第幾個商品名稱
                var id_amount = "#amount_"+id;　// 找出　第幾個數量的id
                var amount = $(id_amount).val(); // 找出　第幾個數量
                var is_num = isNotANumber(amount); // 檢查此數量是否為整數
                if(!is_num||amount<=0){ // 數量必須要>0
                    alert("請輸入正確數量");
                    return false;
                }
                
                var cartCookie = getLocalStorage(); // 目前購物車的情況
                var arrayCartCookie = JSON.parse(cartCookie); // 把購物車情況從json轉為array包obj
                // 如果購物車無此商品和尺寸 直接加入array, 如果購物車有此商品和尺寸 改加數量
                //console.log("###如果購物車無此商品和尺寸 直接加入array, 如果購物車有此商品和尺寸 改加數量###");
                var is_found_product = false;
                for (var i = 0; i < arrayCartCookie.length; i++) {
                    if(arrayCartCookie[i].id==id && arrayCartCookie[i].size==size){        
                        console.log("有此商品");                
                        is_found_product = true; 
                        // 直接加購物車此商品的數量
                        arrayCartCookie[i].amount = (parseInt(arrayCartCookie[i].amount) + parseInt(amount));                        
                    }
                }

                if(!is_found_product){
                    console.log("無此商品");
                    var cart_obj = {}; // 創一個obj
                    cart_obj["id"] = id;
                    cart_obj["size"] = size;
                    cart_obj["price"] = price;
                    cart_obj["amount"] = amount;
                    cart_obj["title"] = title;

                    //console.log(cart_obj);   
                    // 購物車加一個商品
                    arrayCartCookie.push(cart_obj); // 把這個商品的obj丟到商品array裡面    
                }

                // 再把商品array轉成json 紀錄起來
                var jsonCartCookie = JSON.stringify(arrayCartCookie);
                setLocalStorage(jsonCartCookie);
                      

                // 如果是直接購買 就跳出購物車
                var is_buy = $(obj).attr("buy");
                if(is_buy=="1"){
                    ModalCart(); // 跳出購物車視窗

                // 如果只是加入購物車 跳提醒就好
                } else {
                    alert("已加入購物車");
                }

            }


            // 檢查目前的購物車情況
            function checkCurrentCart(){

                // 如果目前購物車完全沒設定　那先給"[]"這個值
                // 在json, []代表空array
                var cartCookie = getLocalStorage();
                if(!cartCookie){                    
                    setLocalStorage("[]");
                    //var newCookie = getLocalStorage();
                    console.log('cart null');
                    //var objNewCookie = JSON.parse(newCookie);                    
                    //console.log(objNewCookie);
                } else {
                    console.log('cart exist');
                    //console.log(cartCookie);
                }
            }


            // 新增一組尺寸
            function plusSpecification(size, price){
                if($(".div_size_edit").length > 9){ // 每個商品最多九組尺寸
                    alert("最多設置九組");
                    return false;
                }
                var html = '';
                html += '<div class="div_size_edit">';
                html += '<input type="text" class="form-control editInputs edit_size" placeholder="尺寸" maxlength="10" value="'+size+'">';
                html += '<input type="number" class="form-control editInputs edit_price" placeholder="單價" maxlength="10" value="'+price+'">';
                html += '<div class="div_del"><button type="button" class="btn btn-danger edit_del">刪除</button></div>';
                html += '</div>';
                
                // append就是往下加html
                $("#div_img_create").append(html);
                // 因為新append的class沒有吃到javascript on click的值
                // 因此要先把其他class on click 的值洗掉 然後再加上去
                // 這樣admin 刪除一組尺寸時 功能才會有效
                loadjscssfile("js/editDel.js","js"); 
            }


            // [網路上找的範例] 動態載入一個js/css檔案 
            function loadjscssfile(filename, filetype){
                if (filetype=="js"){
                var fileref=document.createElement('script')
                  fileref.setAttribute("type","text/javascript")
                  fileref.setAttribute("src",filename)
                } else if (filetype=="css"){
                  var fileref=document.createElement("link")
                  fileref.setAttribute("rel","stylesheet")
                  fileref.setAttribute("type","text/css")
                  fileref.setAttribute("href",filename)
                }

                if (typeof fileref!="undefined"){
                  document.getElementsByTagName("head")[0].appendChild(fileref)
                } 
            }


            // [網路上找的範例] 讀取圖檔
            function readURL(input, img_id) {      
                if (input.files && input.files[0]) {
                        var reader = new FileReader();          
                        reader.onload = function (e) {
                        $(img_id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }


            // 上方nav 顯示未登入
            function showUnloginNav(){
                var html = '';
                html += '<a class="navbar-brand navbar-right" href="#" onclick="ModalLogin()">登入</a>';
                html += '<a class="navbar-brand navbar-right" href="#" onclick="ModalRegister()">註冊</a>';
                html += '<a class="navbar-brand navbar-right" href="#" onclick="ModalCart()">購物車</a>';
                $("#nav_div").html(html);
            }   


            // 上方nav 顯示已登入
            function showLoginNav(){
                var html = '';
                html += '<a class="navbar-brand navbar-right" href="#" onclick="Logout()">登出</a>';
                html += '<a class="navbar-brand navbar-right" href="#" >'+session_account+'</a>';
                html += '<a class="navbar-brand navbar-right" href="#" onclick="ModalOrders()">訂單</a>';
                if(session_account!='ADMIN'){                    
                    html += '<a class="navbar-brand navbar-right" href="#" onclick="ModalCart()">購物車</a>';      
                }                        
                $("#nav_div").html(html);
            }               


            // 跳出註冊視窗
            function ModalRegister(){
                $("#modal_register").modal("toggle"); // 跳出註冊小視窗(modal)
            }


            // 跳出登入視窗
            function ModalLogin(){
                $("#modal_login").modal("toggle");  // 跳出登入小視窗(modal)
            }


            // 跳出訂單視窗
            function ModalOrders(){

              // 畫出訂單 第一頁            
              drawOrderList();

              $("#modal_orders").modal("toggle");  // 跳出登入小視窗(modal)
            }

 
            // 畫出訂單 起始頁面根據 cur_page去改變          
            function drawOrderList(obj){
        
              var one_page_orders = 5; // 一次顯示幾筆訂單

              // 頁數由attr的page決定, 如果undefined(沒有), 就代表第一頁
              var current_page = 1;
              var page = $(obj).attr("page");
              if (typeof page !== 'undefined'){
               current_page = page;
              }
              
              if(current_page<1){ // 頁數最少是第一頁
                current_page = 1;
              }
              
              // 最多能顯示的頁數 = (總訂單數/依次顯示的頁數) ->無條件進位
              var total_pages = Math.round(get_order_array.length / one_page_orders);
              if(current_page>total_pages){ // 如果顯示頁大於最高頁 強制改為最高頁
                current_page = total_pages;
              }

              var i_start = (current_page-1)*one_page_orders; //  訂單第一筆= (目前頁數-1)*一頁顯示多少頁
              if(i_start<0){
                i_start = 0;
              }
              var i_end = i_start +  one_page_orders; //  訂單最後筆= 訂單第一筆 + 一頁顯示多少頁;
              if(i_end > get_order_array.length){  // 如果訂單最後一筆 大於總訂單數 那就要等於總訂單數
                i_end = get_order_array.length;
              }

              // 準備處理 訂單內容
              var html = '';

              // 先要有table上方文字
              html += '<tr>';
              html += '<td>ID</td>';     

              if(session_account=="ADMIN"){  // 管理員才會看到訂單會員
                html += '<td>會員</td>';   
              }
              html += '<td>姓名/手機</td>';
              html += '<td>地址</td>';
              html += '<td>建立時間</td>';    
              html += '<td>金額</td>';                                                         
              html += '<td>付款方式</td>';
              html += '<td>已付款(時間)</td>';
              html += '</tr>'; 

              // 根據起始第幾筆 最後顯示第幾筆 去顯示order資料
              for (var i = i_start; i < i_end; i++) {

                  // 如果資料庫存的付款方式為空 那這邊就不要顯示null 改顯示空白
                  var payment_type = get_order_array[i].payment_type;
                  if(payment_type=='null'||payment_type==null){
                    payment_type = '';
                  }
                  // 如果資料庫存的付款時間 那這邊就不要顯示null 改顯示空白
                  var paid_at = get_order_array[i].paid_at;
                  if(paid_at=='null'||paid_at==null){
                    paid_at = '';
                  }                  

                  // 每個訂單的狀況
                  html += '<tr>';
                  html += '<td>'+get_order_array[i].order_id+'</td>';  // 訂單號  

                  if(session_account=="ADMIN"){  // 管理員才會看到訂單會員
                    var account = get_order_array[i].account; // 會員名稱
                    if(account==null){
                      account = '';
                    }
                    html += '<td>'+account+'</td>';       
                  } // 666
                  
                  html += '<td>'+get_order_array[i].order_name+'<br/>'+get_order_array[i].order_phone+'</td>'; // 購買姓名/電話    
                  html += '<td><span style="word-break:break-all;">'+get_order_array[i].order_address+'</span></td>'; // 購買地址("word-break:break-all強迫換行)

                  html += '<td>'+get_order_array[i].obtained_at+'</td>'; // 訂單建立時間       
                  html += '<td>'+get_order_array[i].total_price+'</td>'; // 訂單總價錢                       
                  html += '<td>'+payment_type+'</td>';     // 訂單付款方式
                  html += '<td>'+paid_at+'</td>';  // 訂單已收款時間
                  html += '</tr>';
              }

              // 到此為止 先把訂單table畫出來
              $("#tbody_orders").html(html);                 


              // 處理下面的頁數
              var html_pager = '';
              html_pager += '<ul class="pager">'; // ul起始

              // 如果目前顯示頁數大於1 才顯示 "上一頁"這顆按鈕
              if(current_page>1){ 
                var pre_page = parseInt(current_page)-1; // 按下上一頁 要跳到 page是目前頁面-1那頁
                html_pager += '<li class="previous"><a href="#" page="'+pre_page+'" onclick="drawOrderList(this)">上一頁</a></li>';  
              }
              

              // 準備跑一個所有頁數的迴圈
              for (var i = 1; i< total_pages+1; i++) {

                // 但這邊限制 只顯示本頁的前後+2頁
                if(!(   i==(parseInt(current_page)-2)
                   || i==(parseInt(current_page)-1)
                   || i==current_page
                   || i==(parseInt(current_page)+1)
                   || i==(parseInt(current_page)+2)
                )){
                  continue; // 如果不是這幾頁  迴圈就不往下走
                }

                var active = ''; 
                if(i==current_page){ // 如果目前顯示的就是這頁 那這頁要換顏色 #ddd
                  active = 'style="background-color:#ddd;"';
                }
                // 頁數
                html_pager += '<li ><a href="#" '+active+' page="'+i+'" onclick="drawOrderList(this)">'+i+'</a></li>';
              }              

              // 如果目前顯示頁數 不等於 最後一頁  才能顯示 "下一頁"這顆按鈕
              if(current_page != total_pages){
                var next_page = parseInt(current_page)+1;    // 按下一頁 要跳到 page是目前頁面+1那頁             
                html_pager += '<li class="next"><a href="#" page="'+next_page+'" onclick="drawOrderList(this)">下一頁</a></li>';
              }
              
              html_pager += '</ul>';
              $("#pager_orders").html(html_pager);
            }


            // 跳出購物車視窗 
            function ModalCart(){

                // 畫出購物車                
                drawCart();
               
                $("#modal_cart").modal("toggle");  // 跳出購物車小視窗(modal)
            }


            // 畫出購物車
            function drawCart(){
                //console.log(groupProduct);

                // 先取得目前購物車情況
                var current_cart = getLocalStorage()
                // 把購物車情控從json轉成array包obj
                current_cart = JSON.parse(current_cart);
                //console.log(current_cart); 

                var html = '';
                var total_price = 0;
                for (var i = 0; i < current_cart.length; i++) {
                  // table的最上層要補標題
                    if(i==0){
                        html += '<tr>';
                        html += '<td style="max-width:60px;">圖片</td>';
                        html += '<td>標題</td>';
                        html += '<td>尺寸</td>';
                        html += '<td>單價</td>';
                        html += '<td>數量</td>';
                        html += '<td>價格</td>';
                        html += '<td></td>';
                        html += '</tr>';                        
                    }
                    //console.log(current_cart[i]);

                    // 我們在ready的時候已經把商品 可以從第幾個去找到他
                    // 所以 groupProduct[1] 會跳出第一個商品
                    // groupProduct[1].image 就是第一個商品的圖片網址
                    // groupProduct[1].title 就是第一個商品的名稱
                    // current_cart[i].id則代表 這是第幾個商品 資料庫已經寫死 就是那四個
                    var img = groupProduct[current_cart[i].id].image;
                    var title = groupProduct[current_cart[i].id].title;
                    var sum = current_cart[i].amount * current_cart[i].price;

                    total_price += sum; // 累加目前購物車總價

                    html += '<tr>';
                    html += '<div class="form-group">';
                    html += '<td style="max-width:60px;"><img src="'+img+'" class="imageCart" style="max-width:100px;max-height:100px;"></td>';
                    html += '<td>'+title+'</td>';                    
                    html += '<td>'+current_cart[i].size+'</td>';
                    html += '<td>'+current_cart[i].price+'</td>';
                    html += '<td><input type="number" class="form-control prd_amount" style="max-width:100px;"  cart_i="'+i+'" value="'+current_cart[i].amount+'"></td>';

                    html += '<td>'+sum+'</td>';
                    html += '<td style="max-width:50px;"><button type="button" class="btn btn-danger" del_cart_id="'+i+'" onclick="delProduct(this)">刪除</button></td>';
                    html += '</div>';
                    html += '</tr>';
                }
                $("#tbody_cart").html(html); // 把購物車畫出來
                $(".total_price").html('總價: '+total_price); // 把購物車總價顯示出來
                $("#cart_price").val(total_price); // 把購物車要傳給金流端的input name 總價填進去

                // 如果有登入 那要把會員 姓名 手機 地址 填入 
                if(session_id!=0){
                  $("#cart_username").val(get_user_info.username);
                  $("#cart_phone").val(get_user_info.phone);
                  $("#cart_address").val(get_user_info.address);
                }

                // 商品數量改變時     
                $(".prd_amount").off("change");  
                $(".prd_amount").change(function(){
                    var new_amount = $(this).val(); // 新的數量
                    var is_num = isNotANumber(new_amount); // 檢查是否是整數
                    if(!is_num||new_amount<=0){ // 數量小於等於0的時候不允許 並且強制=1
                        //alert("請輸入正確數量");
                        $(this).val(1);
                        return false;
                    }             

                    // 如果數量正確 就可以往下繼續
                    // 取得這是購物車裡面第幾個商品
                    var cart_i = $(this).attr("cart_i");  // 上面我們在購物車商品數量  有加這個值 cart_i = i
                
                    // 改變目前購物車 第幾個商品的 數量 並且重新把購物車存入localStorage
                    current_cart[cart_i].amount = new_amount;
                    current_cart = JSON.stringify(current_cart);
                    setLocalStorage(current_cart);   
                    drawCart(); // 商品改變數量 就要重新畫一次購物車
                   
                });
            }


            // 按下刪除(購物車商品)  
            function delProduct(obj){
                var index =$(obj).attr("del_cart_id"); // 取得這是購物車裡面第幾個商品                

                // 取得購物車資訊
                var current_cart = getLocalStorage()
                current_cart = JSON.parse(current_cart);
                  
                // 把購物車的第幾(index)個 從array刪掉
                current_cart.splice(index, 1);  

                // 轉json重新存入localStorage 
                current_cart = JSON.stringify(current_cart);                  
                setLocalStorage(current_cart);            
              
                // 刪除商品之後 重新畫一次購物車
                drawCart();
            }


            // 跳出編輯視窗
            var current_edit_id = 0;  // 目前要編輯的商品id         
            function ModalEdit(obj){
                // 因為在html有寫ModalEdit(this) 所以可以用obj把他的元素撈下來
                current_edit_id = $(obj).attr("value"); // 第幾號商品
                console.log(current_edit_id);

                // 先清乾淨尺寸
                $("#div_img_create").html('');

                // 因為知道是幾號商品 所以可以從 get_product_array 找到資料給 modal_edit
                for (var i = 0; i < get_product_array.length; i++) {
                    if(get_product_array[i].id==current_edit_id){
                        $("#modal_edit_image").attr("src", get_product_array[i].image);
                        $("#modal_edit_title").val(get_product_array[i].title);
                        $("#modal_edit_descript").html(get_product_array[i].descript);
                        console.log(get_product_array[i].title);

                        // 找到這個商品的尺寸 
                        var specific_array = get_product_array[i].specification; 
                        for (var j = 0; j < specific_array.length; j++) {                                            
                                // 把資料庫的尺寸加進去
                                plusSpecification(
                                      specific_array[j].size
                                    , specific_array[j].price
                                );                       
                        }
                    }       
                }

                $("#modal_edit").modal("toggle");  // 跳出編輯小視窗(modal)
            }


            // 編輯 要改資料庫 因此要用ajax連動到 edit.php
            function Edit(){

                var image = $("#upload_edit_img").prop("files")[0]; // 目前編輯商品的新圖(沒有更改 就會是空的)
                var title = $("#modal_edit_title").val(); // 編輯商品的新名稱
                var descript = $("#modal_edit_descript").val(); // 編輯商品的新描述
                var json_array = [];
                var length = $(".div_size_edit").length; // 根據div_size_edit 的class數量 知道目前這個商品有幾個尺寸
                for (var i = 0; i < length; i++) {
                    var obj = {};
                    obj["size"] = $(".edit_size").eq(i).val(); // eq(0)代表這個class的第一個 所以我們可以把i丟到eq 全部掃一次
                    obj["price"] = $(".edit_price").eq(i).val();
                    if(obj["size"]==''||obj["price"]==''){
                        alert("尺寸/價格不能為空白");
                        return false;
                    }
                    json_array.push(obj); // 把新的尺寸obj一個一個丟到array
                }
                
                // 把尺寸資訊轉json
                var specification = JSON.stringify(json_array);

                // 因為我們要傳送圖片 因此要用formData
                var formData = new FormData();
                formData.append('image', image);
                formData.append('title', title);
                formData.append('descript', descript);
                formData.append('specification', specification);
                formData.append('edit_id', current_edit_id);      

                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // javascript連動php  (edit.php)
                $.ajax({
                    url: 'edit.php', // 目標
                    type: 'POST', // 我們都用post傳過去
                    data: formData, // 剛才要傳送的資料 都在formdata
                    processData: false, // 要傳送formData這兩個就是要false
                    contentType: false, // 
                    error: function (error_msg) { // 如果ajax失敗                
                        // 重刷網頁        
                        //location.reload();
                        alert("編輯失敗");
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 重刷網頁                                
                        alert(result);
                        location.reload();
                    }
                });

            }


            // 登出
            function Logout(){

                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // javascript連動php  (logout.php)
                // php那邊可以自己清空登入資訊 所以不用傳甚麼過去
                $.ajax({
                    url: 'logout.php',  // 目標
                    type: 'POST', // 我們都用post傳過去
                    data: {  // 傳過去的東西 空的
                    },
                    error: function (error_msg) { // 如果ajax失敗                
                        // 重刷網頁        
                        location.reload();
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 重刷網頁        
                        location.reload();
                    }
                });

            }


            // 回傳驗證碼
            function confirmSMS(){

                // // 管理員 或是 未登入 應該不會有這種狀況, 但還是擋一下
                // if(session_account=='ADMIN'||session_account==''||get_user_info==''){
                //   alert("請先登入會員");
                //   return false;
                // }

                var account = $("#register_account").val();
                if(vcode==''){  
                  alert("註冊帳號不能為空");
                  return false;
                }

                var vcode = $("#confirm_phone_vcode").val();
                if(vcode==''){  
                  alert("驗證碼不能為空");
                  return false;
                }

                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // 把註冊的 account和 password 傳給register.php
                // 然後等待success的回傳值 (reister.php的echo值)
                $.ajax({
                    url: 'confirm_sms.php',  // 目標
                    type: 'POST',  // 我們都用post傳過去
                    data: {
                      vcode:vcode, // 把簡訊驗證碼傳過去
                      account:account, // 把註冊帳號傳過去
                    }, // 傳值
                    error: function (error_msg) { // 如果ajax失敗
                        // 顯示畫面
                        $("#body_div").show();                        
                        alert(error_msg);
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 把ajax的回傳值附帶的'換行'拿掉
                        var success_result = result.replace(/\r\n|\n/g,"");

                        console.log('回傳結果:'+success_result);

                        if(success_result=="success"){ // 如果註冊成功
                            
                            alert("簡訊驗證成功");
                            location.reload(); // 重刷網頁

                        } else { // 驗證失敗(可能是簡訊端的問題)  

                            alert(result);
                            
                            // 顯示畫面
                            $("#body_div").show();                              
                        }
                      
                    }
                });              
            }


            // 寄出驗證信
            function sendEmail(){

                // 管理員 或是 未登入 應該不會有這種狀況, 但還是擋一下
                if(session_account=='ADMIN'||session_account==''||get_user_info==''){
                  alert("請先登入會員");
                  return false;
                }

                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // 把註冊的 account和 password 傳給register.php
                // 然後等待success的回傳值 (reister.php的echo值)
                $.ajax({
                    url: 'send_email.php',  // 目標
                    type: 'POST',  // 我們都用post傳過去
                    data: {}, // 不用傳值, 因為如果有登入, session有存user_id
                    error: function (error_msg) { // 如果ajax失敗
                        // 顯示畫面
                        $("#body_div").show();                        
                        alert(error_msg);
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 把ajax的回傳值附帶的'換行'拿掉
                        var success_result = result.replace(/\r\n|\n/g,"");

                        console.log('回傳結果:'+success_result);

                        if(success_result=="success"){ // 如果註冊成功
                            
                            alert("寄信成功, 請至信箱收信 (有可能在垃圾/重要信件中)");
                            //location.reload(); // 如果回傳註冊成功 就重刷網頁 讓一開始的php去判斷有沒有登入                        

                        } else { // 如果註冊失敗(帳號重複)  

                            alert(result);
                            
                            // 顯示畫面
                            $("#body_div").show();                              
                        }
                      
                    }
                });              
            }


            // Login 登入
            function Login(){
                var account = $("#login_account").val();
                var password = $("#login_password").val();

                // 如果帳號或密碼為空
                if(account==''||password==''){
                    alert("帳號/密碼 不能為空白");
                    return false;
                }

                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // 把註冊的 account和 password 傳給register.php
                // 然後等待success的回傳值 (reister.php的echo值)
                $.ajax({
                    url: 'login.php',  // 目標
                    type: 'POST',  // 我們都用post傳過去
                    data: {
                        account: account,  // 把帳號命名為 account 傳過去
                        password: password, // 把密碼命名為 password 傳過去
                    },
                    error: function (error_msg) { // 如果ajax失敗
                        // 顯示畫面
                        $("#body_div").show();                        
                        alert(error_msg);
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 把ajax的回傳值附帶的'換行'拿掉
                        var success_result = result.replace(/\r\n|\n/g,"");

                        console.log('回傳結果:'+success_result);

                        if(success_result=="success"){ // 如果註冊成功
                            
                            alert("登入成功");
                            location.reload(); // 如果回傳註冊成功 就重刷網頁 讓一開始的php去判斷有沒有登入                        

                        } else { // 如果註冊失敗(帳號重複)  

                            alert(result);
                            
                            // 顯示畫面
                            $("#body_div").show();                              
                        }
                      
                    }
                });
            }            

            // 註冊
            function Register(obj){

                // 註冊類型
                var reg_type = $(obj).attr("reg_type");

                // 註冊內容
                var account  = $("#register_account").val();
                var password = $("#register_password").val();
                var username = $("#register_username").val();
                var address  = $("#register_address").val();
                var phone    = $("#register_phone").val();                
                var mail     = $("#register_mail").val();

                // 如果帳號或密碼為空
                if(account==''||password==''||username==''||phone==''||address==''||mail==''){
                    alert("個人資料不得為空白");
                    return false;
                }                

                // 檢查手機規格
                var is_phone_legal = checkPhoneFormat(phone);
                if(!is_phone_legal){
                  alert("手機號碼不合法"); 
                  return false;
                }

                // 檢查mail規格
                var is_mail_legal = checkMailFormat(mail);
                if(!is_mail_legal){
                  alert("信箱不合法"); 
                  return false;
                }                

                // 如果驗證方式是手機 那email改填none
                if(reg_type=='phone'){
                  mail = 'none';
                }

                // 如果驗證方式是email 那手機改填none
                if(reg_type=='mail'){
                  phone = 'none';
                }


                // 隱藏畫面(等待回應再打開)
                $("#body_div").hide();

                // 把註冊的 account和 password 傳給register.php
                // 然後等待success的回傳值 (reister.php的echo值)
                $.ajax({
                    url: 'register.php',  // 目標
                    type: 'POST',  // 我們都用 post傳過去
                    data: {
                        reg_type:reg_type, // 把註冊類型 命名為reg_typet傳過去                      
                        account: account, // 把帳號命名為 account 傳過去
                        password: password,  // 把密碼命名為 password 傳過去
                        username: username,  // 把名字命名為 username 傳過去
                        phone: phone,  // 把電話命名為 phone 傳過去
                        address: address,  // 把地址命名為 address 傳過去
                        mail: mail,  // 把Email命名為 mail 傳過去                        
                    },
                    error: function (error_msg) { // 如果ajax失敗
                        // 顯示畫面
                        $("#body_div").show();                        
                        alert(error_msg);
                    },
                    success: function (result) // 如果ajax成功
                    {
                        // 把ajax的回傳值附帶的'換行'拿掉
                        var success_result = result.replace(/\r\n|\n/g,"");

                        //console.log('回傳結果:'+success_result);

                        // 顯示畫面
                        $("#body_div").show(); 

                        // 如果Email註冊 目前成功
                        if(success_result=="success_email"){
                            
                          // 關閉modal
                          $('#modal_register').modal('hide');

                          alert("驗證信將於五分鐘內寄出, 驗證請檢查您的信件(或垃圾信箱)");                     

                        // 如果手機註冊 目前成功
                        } else if (success_result=="success_phone"){

                          alert("請至手機接收簡訊\n於下方填上驗證碼\n再按下'完成手機驗證註冊'");                          

                        } else { // 如果註冊失敗(帳號重複)  

                          // 跳出失敗訊息
                          alert(result);
                                                       
                        }
                      
                    }
                });
            }
                 

            // 把購物車的物品串金流付款
            function submitCart(){

              var username = $("#cart_username").val();
              var phone    = $("#cart_phone").val();
              var address  = $("#cart_address").val();

              // 如果帳號或密碼為空
              if(username==''||phone==''||address==''){
                  alert("聯絡資訊不得為空白 請輸入您的完整資訊 或 登入您的會員");
                  return false;
              }                

              // 檢查手機規格
              var is_phone_legal = checkPhoneFormat(phone);
              if(!is_phone_legal){
                alert("手機號碼不合法"); 
                return false;
              }

              // 檢查有沒有商品
              var cartCookie = getLocalStorage();
              var arrayCartCookie = JSON.parse(cartCookie);
              if(arrayCartCookie.length==0){
                  alert("購物車沒有商品");
                  return false;
              }

              // 真正submit post出去的有三個值
              // 價格(cart_price)已經在drawCart()處理好了
              $("#cart_json").val(cartCookie);
              $("#cart_user_id").val(session_id);

              // 清空購物車localStorage的購物車資訊
              setLocalStorage('[]');

              // 綠界要求前端用form submit傳資料給他寫好的php檔
              // payForm的action已經寫好要送過去的目標php檔案 sample_All_CreateOrder.php
              // 同時這個form已經設定使用 post
              $("#payForm").submit()           
            }


            // 設置 LocalStorage
            function setLocalStorage(cvalue)
            {
              // 原本用cookie紀錄購物車資訊
              // 但是因為讓手機可以用
              // 所以改用localStorage
              var cname = "sotrageNametest1";
              localStorage.setItem(cname,cvalue);  
            }


            // 取得 LocalStorage
            function getLocalStorage()
            {
              // 原本用cookie紀錄購物車資訊
              // 但是因為讓手機可以用
              // 所以改用localStorage  
              var cname = "sotrageNametest1";
              var result = localStorage.getItem(cname);
              return result;
            }


            // 判斷是否為數字
            function isNotANumber(inputData) { 
            　　//isNaN(inputData)不能判斷空串或一個空格 
            　　//如果是一個空串或是一個空格，而isNaN是做為數字0進行處理的，而parseInt與parseFloat是返回一個錯誤訊息，這個isNaN檢查不嚴密而導致的。 
            　　if (parseFloat(inputData).toString() == "NaN") { 
            　　　　//alert(“請輸入數字……”); 
            　　　　return false; 
            　　} else { 
            　　　　return true; 
            　　} 
            }            

            // 判斷電話號碼是否合法
            function checkPhoneFormat(str){

              var reg = /[0-9]{8}$/; // 09後面接數字
              if (reg.test(str)) {
                  return true;
              } else {
                  return false;
              }              
            }


            // 判斷EMail是否合法
            function checkMailFormat(inputData) {
              const re = /^(([.](?=[^.]|^))|[\w_%{|}#$~`+!?-])+@(?:[\w-]+\.)+[a-zA-Z.]{2,63}$/;
              if (re.test(inputData))
              {
                return true;
              }   
              else {
                return false;
              }  
            }


        </script>
        <!-- 這個網頁的的javaScript程式 結束 -->   


    </body>
    <!-- body 結束 -->


</html>
