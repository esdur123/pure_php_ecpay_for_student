<?php
/**
 * 註冊
 */


    // 使用session
    session_start();

	// 把傳過來的兩個值 先處理
    $reg_type      = $_POST['reg_type']; // 註冊類型
	$post_account  = strtoupper($_POST['account']); // 註冊帳號 (強制大寫strtoupper)
	$post_password = $_POST['password']; // 註冊密碼
    $post_username = $_POST['username']; // 註冊姓名
    $post_address  = $_POST['address']; // 註冊地址
    $post_phone    = $_POST['phone']; // 註冊電話
    if($reg_type=='email'){
        $post_phone = "none";
    }    
    
    $post_email    = strtoupper($_POST['mail']); // 註冊Email (強制大寫strtoupper)    
    if($reg_type=='phone'){ // 如果驗證方式是手機 那email改填none
        $post_email = "none";
    } 

    
    include_once 'dbconnect.php'; // 連線資料庫
    include_once 'defined.php'; // domain名稱在這邊取得 my_domain_name

    // 檢查有沒有重複的帳號 (註冊會強制把帳號轉大寫 所以沒有大小寫問題)
    $sql = "SELECT * FROM `".$db_name."`.`user` ;"; // sql指令, 取得所有的user資料庫資訊
    $result = mysqli_query($conn, $sql) or die(mysqli_error()); // 把sql指令丟給資料庫執行 如果錯誤會回報error
    while ($find_row = mysqli_fetch_array($result)) { // 把剛才從資料庫撈的資料用while迴圈做檢查(已取得所有user帳號)
    	if(strtoupper($find_row['account'])==strtoupper($post_account)&&$find_row['is_verified']=='1'){ // 如果發現此帳號已被註冊
    		echo "註冊失敗 已有相同帳號被註冊"; // 回傳給首頁的ajax success
    		exit; // 強制讓這個.php檔結束
    	}

        if(strtoupper($find_row['phone'])==strtoupper($post_phone)&&$reg_type=='phone'){ // 如果發現此phone已被註冊
            echo "註冊失敗 已有相同手機號被註冊"; // 回傳給首頁的ajax success
            exit; // 強制讓這個.php檔結束
        }

        if(strtoupper($find_row['email']==$post_email)&&$reg_type=='email'){ // 如果發現此email已被註冊
            echo "註冊失敗 已有相同Email被註冊"; // 回傳給首頁的ajax success
            exit; // 強制讓這個.php檔結束
        }
    }

    // 如果沒有重複帳號 
    // 產生會員的驗證碼 timestamp + 4碼隨機
    $vcode_email = time() . substr(strval(rand(10000,19999)),1,4);
    $vcode_phone = substr(strval(rand(10000,19999)),1,4);

    // sql指令, 把會員資料寫入資料庫
    $insert_sql = "INSERT INTO `".$db_name."`.`user` "
    . "(`account`,`password`,`username`,`phone`,`address`,`email`,`vcode_email`,`vcode_phone`) " 
    . "VALUES ('".$post_account."','".$post_password."','".$post_username."','".$post_phone."','".$post_address."','".$post_email."','".$vcode_email."','".$vcode_phone."');";

    // 執行sql指令
    if(mysqli_query($conn, $insert_sql)){
        // 如果剛才創一筆user成功 那我們用下面這行取得剛才創的user id (last_insert_id)
        $last_insert_id = mysqli_insert_id($conn);
    } else {
        // 如果剛才創order失敗
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        exit;
    }


    // 如果是Email註冊
    if($reg_type=='email'){

        // 寄信
        $url = $my_domain_name.'/confirm_email.php?vcode='.$vcode_email;
        $msg = "親愛的會員".$post_account."，您好\n\n請按下連結驗證您的Email\n\n".$url;
        $msg = wordwrap($msg,200);
        mail($post_email,"驗證Email信箱",$msg);

        // 回傳 Email驗證目前成功
        echo "success_email"; 
        exit;       

    }

    // 如果是手機註冊
    if($reg_type=='phone'){

        // 簡訊        
        $txt = $post_account."您好, 網站手機驗證碼 ".$vcode_phone;

        // 傳送簡訊
        $result = sendByTWSMS(
                       $twsms_username
                     , $twsms_password
                     , '886'
                     , $post_phone
                     , $txt              
                     );    

        if($result=='0000'){
         echo "success_phone"; 
        } else {
         echo '傳送簡訊失敗 錯誤代碼: '.$result;
        }
        exit;
    }


    // 台灣簡訊
    function sendByTWSMS($sms_username, $sms_password, $country_code, $phone, $msg)
    {

        /** 沒0補0 */
        if (!preg_match('/^0/', $phone)) {
            $phone = "0" . $phone;
        }

        /** 只發送手機 */
        if (!preg_match('/^09\d{8}$/', $phone)) {
            return false;
        }

        $data = array(
            "username" => $sms_username,
            "password" => $sms_password,
            "mobile" => $phone,
            "message" => urlencode($msg));

        $post = http_build_query($data, null, '&', PHP_QUERY_RFC3986);
        $url = "http://api.twsms.com/smsSend.php?" . $post;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        /*
        00000 完成
        00001 狀態尚未回復
        00010 帳號或密碼錯誤
        00020 通數不足
        00030 IP 無使用權限
        00040 帳號已停用
        00050 sendtime 格式錯誤
        00060 expirytime 格式錯誤
        00070 popup 格式錯誤
        00080 mo 格式錯誤
        00090 longsms 格式錯誤
        00100 手機號碼格式錯誤
        00110 沒有簡訊內容
        00120 長簡訊不支援國際門號
        00130 簡訊內容超過長度
        00140 drurl 格式錯誤
        00150 sendtime 預約的時間已經超過
        00300 找不到 msgid
        00310 預約尚未送出
        00400 找不到 snumber 辨識碼
        00410 沒有任何 mo 資料
        99998 資料處理異常，請重新發送
        99999 系統錯誤，請通知系統廠商
         */
        return $array['code'];
    }

    //echo "success_".$reg_type; // 回傳給首頁的ajax success

?>

