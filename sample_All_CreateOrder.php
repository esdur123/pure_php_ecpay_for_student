<?php
/**
* 綠界提供的一般產生訂單(全功能)範例
* 我們有修改一部分
*/
    
    // 使用session
    session_start();

    // 前端post過來的form資訊
    $post_session_id = $_POST['cart_user_id'];    // 誰下的訂單 (資料庫user的id)
    $post_price      = (int)$_POST['cart_price']; // 此訂單總價
    $post_products   = $_POST['cart_json'];       // 此訂單有甚麼商品(前端用json傳過來)

    $post_username   = $_POST['cart_username']; // 購買人姓名
    $post_phone      = $_POST['cart_phone'];    // 購買人電話
    $post_address    = $_POST['cart_address'];  // 購買人地址
    
    // 時區
    date_default_timezone_set("Asia/Taipei");
    
    // 連線資料庫 
    include_once 'dbconnect.php';   

    // 創order的sql  創一筆order資料 其中obtained_at用date(php內建取得目前時間)
    $insert_sql = "INSERT INTO `".$db_name."`.`orders` "
    . "(`user_id`,`total_price`,`cart_json`,`obtained_at`,`order_name`,`order_phone`,`order_address`) "
    . "VALUES ('".$post_session_id."','".$post_price."','".$post_products."','".date("Y-m-d H:i:s")."','".$post_username."','".$post_phone."','".$post_address."');";

    // 執行sql指令
    if(mysqli_query($conn, $insert_sql)){
        // 如果剛才創一筆order成功 那我們用下面這行取得剛才創的order id (last_insert_id)
        $last_insert_id = mysqli_insert_id($conn);
    } else {
        // 如果剛才創order失敗
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        exit;
    }

    
    //載入SDK(路徑可依系統規劃自行調整)
    include('defined.php'); // 這行是我們自己加的, 取得統一寫好的綠界各種商店資訊key, iv 商店號碼 那些..
    include('ECPay.Payment.Integration.php'); // 綠界提供的檔案, 等等ECPay_AllInOne()和CheckOut()這兩個function, 來自這隻檔案
    try {
        
    	$obj = new ECPay_AllInOne();
   
        //服務參數
        $obj->ServiceURL  = $ECPayInfo_ServiceURL;  // 串金流網址 從ECPayInfo.php取得
        $obj->HashKey     = $ECPayInfo_HashKey;     // 測試用Hashkey，從ECPayInfo.php取得
        $obj->HashIV      = $ECPayInfo_HashIV;      // 測試用HashIV，從ECPayInfo.php取得 
        $obj->MerchantID  = $ECPayInfo_MerchantID;  // 測試用MerchantID，從ECPayInfo.php取得
        $obj->EncryptType = $ECPayInfo_EncryptType; // CheckMacValue加密類型，從ECPayInfo.php取得


        //基本參數 綠界給的範例大致長這樣 有幾個我們自訂的 (請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = $ECPayInfo_ReturnURL;                       //(自訂)付款完成通知回傳的網址, 從ECPayInfo.php取得
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                           //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                        //交易時間
        $obj->Send['TotalAmount']       = $post_price;                                //交易金額
        $obj->Send['TradeDesc']         = "test" ;                                    //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::ALL ;                  //付款方式:全功能
        $obj->Send['CustomField1']      = $last_insert_id;                            //(自訂)這是我們可以自由傳的值, 這邊傳送訂單id
        $obj->Send['ClientBackURL']     = $ECPayInfo_ClientBackURL;                   //(自訂)訂單完成之後 「返回商店」按鈕 要回到的網頁


        // 根據前端傳來的訂單商品資料 
        // 依照綠界要的格式 包好給綠界
        $post_products = json_decode($post_products); // 把商品資訊的json轉成array包obj
        for ($i=0; $i < count($post_products); $i++) { 
            // 綠界要的商品格式
            $array_item = array(); 
            $array_itemp['Name'] = $post_products[$i]->title.' ('.$post_products[$i]->size.')'; // 商品名稱+(尺寸)
            $array_itemp['Price'] = $post_products[$i]->price;
            $array_itemp['Currency'] = '元';
            $array_itemp['Quantity'] = (int)$post_products[$i]->amount;
            $array_itemp['URL'] = 'dedwed'; // 綠界就這樣寫的dedwed 沒改

            array_push($obj->Send['Items'], $array_itemp); // 把其中一個商品資訊丟到 " $obj->Send['Items'] " 這個Array裡面
        }

        // 這是綠界範例的模式 我們上面那段根據這些改的
        //array_push($obj->Send['Items'], array('Name' => "歐付寶黑芝麻豆漿", 'Price' => (int)"2000",
        //          'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

        # 電子發票參數
        /*
        $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
        $obj->SendExtend['RelateNumber'] = "Test".time();
        $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
        $obj->SendExtend['CustomerPhone'] = '0911222333';
        $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
        $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
        $obj->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach ($obj->Send['Items'] as $info)
        {
            array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
        }
        $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
        $obj->SendExtend['DelayDay'] = '0';
        $obj->SendExtend['InvType'] = ECPay_InvType::General;
        */


        // 產生訂單(auto submit至ECPay) checkoup這個function在ECPay.Payment.Integration.php裡面 
        // 我們上面有引用了
        $obj->CheckOut();
      

    
    } catch (Exception $e) {
    	echo $e->getMessage();
    } 


 
?>