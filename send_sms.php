<?php


    // 使用session
    session_start();
    if($_SESSION['loginuser_account']=='ADMIN'||$_SESSION['loginuser_account']==''){ // 限制只有ADMIN這個帳號能往下走
        echo "未登入會員";
        exit;
    }

    // 連線資料庫
    include_once 'dbconnect.php';
    include_once 'defined.php'; // 簡訊帳密參數在這邊取得

    // 找到此帳號帳號 
    $user_account = "";
    $user_phone = "";
    $sql = "SELECT * FROM `".$db_name."`.`user` where id='".$_SESSION['loginuser_id']."' ;"; // sql指令, 取得所有的user資料庫資訊
    $result = mysqli_query($conn, $sql) or die(mysqli_error()); // 把sql指令丟給資料庫執行 如果錯誤會回報error
    while ($find_row = mysqli_fetch_array($result)) { // 把剛才從資料庫撈的資料用while迴圈做檢查(已取得所有user帳號)
        $user_account = $find_row['account'];
        $user_phone   = $find_row['phone'];
    }


    if($user_account!=""){ // 如果有找到此會員

        // 隨機4位碼
        $random4 = substr(strval(rand(10000,19999)),1,4);

        // 修改會員的簡訊驗碼SQL
        $sql_update = "UPDATE `".$db_name."`.`user` 
                SET vcode_phone='".$random4."' 
                WHERE id='".$_SESSION['loginuser_id']."';
               "; // sql指令, 取得所有的user資料庫資訊

        // 執行
        mysqli_query($conn, $sql_update) or die(mysqli_error()); 

        // 簡訊        
        $txt = $user_account."您好, 網站手機驗證碼 ".$random4;

        // 傳送簡訊
        $result = sendByTWSMS(
                       $twsms_username
                     , $twsms_password
                     , '886'
                     , $user_phone
                     , $txt              
                     );    

        if($result=='0000'){
         echo "請至手機接收簡訊驗證碼";
        } else {
         echo '傳送簡訊失敗 錯誤代碼: '.$result;
        }

    }


    // 台灣簡訊
    function sendByTWSMS($sms_username, $sms_password, $country_code, $phone, $msg)
    {

        /** 沒0補0 */
        if (!preg_match('/^0/', $phone)) {
            $phone = "0" . $phone;
        }

        /** 只發送手機 */
        if (!preg_match('/^09\d{8}$/', $phone)) {
            return false;
        }

        $data = array(
            "username" => $sms_username,
            "password" => $sms_password,
            "mobile" => $phone,
            "message" => urlencode($msg));

        $post = http_build_query($data, null, '&', PHP_QUERY_RFC3986);
        $url = "http://api.twsms.com/smsSend.php?" . $post;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        /*
        00000 完成
        00001 狀態尚未回復
        00010 帳號或密碼錯誤
        00020 通數不足
        00030 IP 無使用權限
        00040 帳號已停用
        00050 sendtime 格式錯誤
        00060 expirytime 格式錯誤
        00070 popup 格式錯誤
        00080 mo 格式錯誤
        00090 longsms 格式錯誤
        00100 手機號碼格式錯誤
        00110 沒有簡訊內容
        00120 長簡訊不支援國際門號
        00130 簡訊內容超過長度
        00140 drurl 格式錯誤
        00150 sendtime 預約的時間已經超過
        00300 找不到 msgid
        00310 預約尚未送出
        00400 找不到 snumber 辨識碼
        00410 沒有任何 mo 資料
        99998 資料處理異常，請重新發送
        99999 系統錯誤，請通知系統廠商
         */
        return $array['code'];
    }

   
	
?>