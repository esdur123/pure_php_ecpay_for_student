<?php
    
    include_once 'dbconnect.php'; // 連線資料庫
    include_once 'defined.php'; // domain名稱在這邊取得 my_domain_name
      
    $vcode = $_GET['vcode']; // 從url的get取得驗證碼

    // 依驗證碼 檢查檢查有沒有此會員
    $is_success = "0"; // 是否驗證成功, 驗證值會傳到JavaScript的Ready去接
    $sql = "SELECT * FROM `".$db_name."`.`user` where vcode_email='".$vcode."' ;"; // sql指令, 取得所有的user資料庫資訊
    $result = mysqli_query($conn, $sql) or die(mysqli_error()); // 把sql指令丟給資料庫執行 如果錯誤會回報error
    while ($find_row = mysqli_fetch_array($result)) { // 把剛才從資料庫撈的資料用while迴圈做檢查(已取得所有user帳號)

        // 如果有此會員 修改驗證email為已驗證
        $sql_update = "UPDATE `".$db_name."`.`user` 
                SET verified_email='1',is_verified='1'   
                WHERE vcode_email='".$vcode."';
               ";

        // 執行SQL
        mysqli_query($conn, $sql_update) or die(mysqli_error()); 

        // 驗證成功
        $is_success = "1";
    }

?>


<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>驗證Email</title>

        <!-- Bootstrap core CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" />

    </head>
    <body>

        <br />
        <a href="index.php">返回首頁</a>

        <br /><br />

        <div class="alert alert-success" style="display: none;">
          <strong>信箱驗證成功!</strong>
        </div>


        <div class="alert alert-danger" style="display: none;">
          <strong>會員不存在 驗證失敗</strong>
        </div>

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script>         
          $(document).ready(function () {
                console.log("Ready");   

                // 取得 是否驗證成功
                var is_success = '<?php echo $is_success; ?>';   

                if(is_success=='1'){ // 如果成功
                    $(".alert-success").show(); // 顯示成功文字div
                    $(".alert-danger").hide();  // 隱藏失敗文字div

                } else { 如果失敗
                    $(".alert-success").hide(); // 顯示失敗文字div
                    $(".alert-danger").show();  // 隱藏成功文字div
                }
            });
                       
        </script>

    </body>
</html>
