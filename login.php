<?php
/**
 * 登入
 */

    // 使用session
    session_start();

	// 把傳過來的兩個值 先處理
	$post_account  = strtoupper($_POST['account']); // 帳號 (強制大寫strtoupper)
	$post_password = $_POST['password']; // 密碼


    // 連線資料庫
    include_once 'dbconnect.php';

    // 檢查有沒有此帳號密碼 (註冊會強制把帳號轉大寫 所以沒有大小寫問題)
    $is_found = false;
    $sql = "SELECT * FROM `".$db_name."`.`user` "; // sql指令, 取得所有的user資料庫資訊
    $result = mysqli_query($conn, $sql) or die(mysqli_error()); // 把sql指令丟給資料庫執行 如果錯誤會回報error
    while ($find_row = mysqli_fetch_array($result)) { // 把剛才從資料庫撈的資料用while迴圈做檢查(已取得所有user帳號)
        // 如果有找到此帳密碼, 同時通過驗證
    	if($find_row['account']==$post_account&&$find_row['password']==$post_password&&$find_row['is_verified']=='1'){ 	
            $_SESSION['loginuser_account'] = $post_account; // 紀錄session登入acccount
            $_SESSION['loginuser_id']      = $find_row['id']; // 紀錄session登入id
            echo "success"; // 回傳給首頁的ajax success
    		exit; // 強制讓這個.php檔結束
    	}
    }

    // 如果沒有此帳密
    echo "帳號或密碼錯誤"; // 回傳給首頁的ajax 

?>

