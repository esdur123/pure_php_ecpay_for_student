<?php
/**
 * 定義一些參數/簡訊/和綠界提供的測試帳號相關
 * 自己開店就改這邊
 * 串接金流網址也寫在這
 * 使用台灣簡訊 https://www.twsms.com/
 */

	// Domain名稱 我們把Defind也寫在這
	$my_domain_name  = "http://ce-studio.tw/A_phpcart"; 

	
	$twsms_username = "esdur123"; // 台灣簡訊帳號
	$twsms_password = "aaaa1234"; // 台灣簡訊密碼	 


	// 綠界商家資料 (目前使用綠界提供的測試號)
	$ECPayInfo_HashKey     = '5294y06JbISpM5x9' ;  //測試用Hashkey，請自行帶入ECPay提供的HashKey       
	$ECPayInfo_HashIV      = 'v77hoKGq4kWxNNIS' ;  //測試用HashIV，請自行帶入ECPay提供的HashIV 
	$ECPayInfo_MerchantID  = '2000132' ;           //測試用MerchantID，請自行帶入ECPay提供的MerchantID
	$ECPayInfo_EncryptType = '1' ;                 //CheckMacValue加密類型，請固定填入1，使用SHA256加密
	$ECPayInfo_ServiceURL  = 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5'; //測試環境(串接金流網址)
	//$ECPayInfo_ServiceURL  = 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5';     //正式環境(串接金流網址)


	// 綠界回傳給我們 串接結果的網址 
	// 這是目前測試的domain+檔案(return.php)
	$ECPayInfo_ReturnURL   = $my_domain_name.'/return.php' ;   

	// 訂單完成之後 「返回商店」按鈕 要回到的網頁
	$ECPayInfo_ClientBackURL   = $my_domain_name.'/index.php' ;  

?>


