<?php
    // 使用session
    session_start();
    include_once 'dbconnect.php'; // 連線資料庫
    include_once 'defined.php'; // domain名稱在這邊取得 my_domain_name
      
    $post_vcode = $_POST['vcode']; // 簡訊驗證碼
    $post_account = strtoupper($_POST['account']); // 帳號 (強制大寫strtoupper)

    $is_pass_vcode = false; // 驗證結果
    $is_found_account = false;
    $user_id = "";

    // 檢查是否有此user並且驗證碼相同
    $sql = "SELECT * FROM `".$db_name."`.`user` where account='".$post_account."' ;"; // sql指令, 取得所有的user資料庫資訊
    $result = mysqli_query($conn, $sql) or die(mysqli_error()); // 把sql指令丟給資料庫執行 如果錯誤會回報error
    while ($find_row = mysqli_fetch_array($result)) { // 把剛才從資料庫撈的資料用while迴圈做檢查(已取得所有user帳號)
        $is_found_account = true;
        $user_id = $find_row['id'];
        if($post_vcode==$find_row['vcode_phone']){
            $is_pass_vcode = true;
        }
    }    

    // 如果沒有此帳號
    if($is_found_account==false){
        echo "尚未手機驗證註冊";
        exit;
    }

    // 如果通過驗證
    if($is_pass_vcode==true){

        // 如果有此會員 修改驗證手機為已驗證
        $sql_update = "UPDATE `".$db_name."`.`user` 
                SET verified_phone='1',is_verified='1'  
                WHERE account='".$post_account."';
               ";

        // 執行SQL
        mysqli_query($conn, $sql_update) or die(mysqli_error());    


        // 順便紀錄session登入
        $_SESSION['loginuser_account'] = $post_account;
        $_SESSION['loginuser_id'] = $user_id;         

        echo "success";

    // 如果驗證失敗
    } else {
        echo "簡訊驗證失敗";
    }


?>

